<?php

// autoload_classmap.php @generated by Composer

$vendorDir = dirname(dirname(__FILE__));
$baseDir = dirname($vendorDir);

return array(
    'Autorize' => $baseDir . '/Models/Autorize.php',
    'Main' => $baseDir . '/Models/Main.php',
    'Models\\Comment' => $baseDir . '/Models/Comment.php',
    'Models\\Db' => $baseDir . '/Models/Db.php',
    'Models\\User' => $baseDir . '/Models/User.php',
    'Models\\Validation' => $baseDir . '/Models/Validation.php',
    'Register' => $baseDir . '/Models/Register.php',
);
