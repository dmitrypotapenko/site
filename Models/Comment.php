<?php
namespace Models;

use Models\Db as Db;

class Comment extends Db
{
	public $id;
	public $user_id;
	public $comment;
	public $datetime;
	public $comments = [];

	public function addComment ()
	{
		$ins = $this->dbConnection->prepare('INSERT INTO comments(`user_id`,`comment`) VALUES(:user_id, :comment)');
		$ins->execute(array('user_id' => $this->user_id, 'comment' => $this->comment));
		$this->id = $this->dbConnection->lastInsertId();
		return $this->id;
	}

	public function findAll()
	{
		$ins = $this->dbConnection->prepare("SELECT * FROM comments ORDER BY id DESC"); //сортировка по убыванию  
		$ins->execute();
		$comments=$ins->fetchAll();
		return $comments;
	}
}	
?>