<?php      

use Models\Comment as Comment;

class Main
{
	public static function getComments()
	{
		$comment = new Comment();
		$notes = [];
		$elm = [];
		if (isset($_POST['Exit'])) {
				unset($_SESSION['user_id']);
				unset($_SESSION['login']);
				session_destroy();
		}  
				
		if (empty($_SESSION['user_id'])) {
			header("location: /autorization_handler.php"); //если id уже есть, значит пользователь авторизирован
		}

		$comment->user_id = $_SESSION['user_id'];
		$comment->comment = $_POST['comment'];

		if(!empty($_POST['comment']) ?? isset($_POST['Save'])) {
			$currentid = $comment->addComment();
			$notes = $comment->findAll();
		}
		return $notes;
	}
}