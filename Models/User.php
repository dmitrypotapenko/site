<?php
namespace Models;

class User extends Db
{
public $id,$name,$surname,$age, $sex, $login, $password, $email;
public $faillog = 0;
public $failemail = 0;
public $autoriz = [];
public $uselogin;

   public function addUser()
    {
        $ins = $this->dbConnection->prepare('INSERT INTO users(`name`,`surname`,`age`,`sex`,`login`,`password`,`email`) VALUES(:name, :surname, :age, :sex, :login, :password, :email)');

            $ins-> execute(array('name' => $this->name, 'surname' => $this->surname, 'age' => $this->age,  'sex' => $this->sex, 
                'login' => $this->login, 'password' => $this->password, 'email' => $this->email));

        $this->id = $this->dbConnection->lastInsertId();
        return $this->id;   //возвращаем id-шник последнего добавленного юзера
    }

    public function find($id)  //поиск по id
    {
        $ins = $this->dbConnection->prepare('SELECT * FROM users WHERE id = :id');
        $ins->execute(array('id' => $id));
        $user = $ins->fetch(\PDO::FETCH_LAZY);
        if(!empty($user))
        {
            $this->id = $id;
            $this->name=$user->name;
            $this->surname = $user->surname;
            $this->age = $user->age;
            $this->sex = $user->sex;
            $this->login = $user->login;
            $this->password = $user->$password;
            $this->email = $email;
            return $this; // возвращаем объект данного класса
        }
    }


   public function checkLogin($login) //для регистрации
    {
        $this->login = $login;

        $ins = $this->dbConnection->prepare('SELECT login FROM users WHERE login = :login'); 
        $ins->execute(array('login' => $this->login)); 
        $reglogin = $ins->fetch(\PDO::FETCH_LAZY);

        if(!empty($reglogin))
        {
            $userlogin = $reglogin->login;
            return $userlogin;
        } else {
            return null;
        }
    }

    public function checkEmail($email) //для регистрации
    {
        $this->email = $email;

        $ins = $this->dbConnection->prepare('SELECT email FROM users WHERE email = :email'); 
        $ins->execute(array('email' => $this->email)); 
        $regemail = $ins->fetch(\PDO::FETCH_LAZY);

        if(!empty($regemail))
        {
            $useremail = $regemail->email;
            return $useremail;
        } else {
            return null;
        }
    }
    

    public function checkAutoriz($login,$password)   //для авторизации
    {
        $this->password = $password;
        $this->login = $login;

        $ins = $this->dbConnection->prepare('SELECT id,login FROM users WHERE login = :login AND password = :password'); 
        $ins->execute(array('login' => $this->login, 'password' => $this->password)); 
        $user = $ins->fetch(\PDO::FETCH_LAZY);

        if(!empty($user))
        {
            $autoriz = array('id' => $user->id, 'login' => $user->login);
            return $autoriz; // возвращаем объект данного класса
        } else{
            return null;
        }     
    }
}
?>