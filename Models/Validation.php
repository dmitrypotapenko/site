<?php
namespace Models;

class Validation {

public $name,$value,$password,$confirm_password;
public $errors = [];
private $db;

public function __construct($db){
		$this->dbConnection = $db;
	}

	public function check ($name, $value)
	{
		$name = ucfirst(str_replace("_", " ", $name));

		if(empty($value)){
			return $this->errors[]= 'Пожалуйста введите'.$name;}
			elseif($name == "confirm_password") {return $this->errors[]= 'Пожалуйста подтвердите введенный пароль'; }
		else {
			return 0;
		}
	}
        
	public function checkMaxLength ($name, $value, $table, $column)
	{
		$name = ucfirst(str_replace("_", " ", $name));
		$maxlength = $this->dbConnection->getMaxLength($table,$column);

		if(strlen($value) > $maxlength) {
			// echo "<br>".strlen($value).' < '. $maxlength.'<br>';
			$this->errors[] = "Поле " .$name." очень длинное. Максимальная длина =  ".$maxlength." символов";
		} else {
			return 0;
		}
	}

	public function checkMinLength ($name, $value, $minLength)
	{
		$name = ucfirst(str_replace("_", " ", $name));
		
        if(strlen($value) < $minLength) {
			return $this->errors[] = "Поле " .$name." должно содержать ".$minLength." символов";
		} else {
			return 0;
		}
	}

	public function checkcoincidence ($password,$confirm_password)
	{
		if($password !== $confirm_password) {return $this->errors[] = 'Пароли не совпадают';}
		else {
			return 0;
		}
	}
}
?>