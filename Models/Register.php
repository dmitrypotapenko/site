<?php
use Models\User as User;
use Models\Validation as Validation;
use Models\Db as Db;

class Register
{
	public static function getRegister()
	{
		$db = new Db();
		$user = new User();
		$valid = new Validation(new Db());
		$errors = [];

	if (!empty($_SESSION['user_id'])) {
		header("location: /Main.php"); //если id уже есть, значит пользователь авторизирован
	}


if (!empty($_POST))
{	

foreach($_POST as $k => $v){
		$valid->check($k, $v);
	}

	$valid->checkMaxLength("user_name", $_POST['user_name'],'users','name');
	$valid->checkMaxLength("surname", $_POST['surname'],'users','surname');
	$valid->checkMaxLength("login", $_POST['login'],'users','login');
	$valid->checkMinLength("password",$_POST['password'],6);
	$valid->checkcoincidence($_POST['password'],$_POST['confirm_password']);

	$errors = $valid->errors;

		if (empty($errors)) {
			// function(){alert('Регистрация выполнена успешно');};
			$user->name = $_POST['user_name'];
			$user->surname = $_POST['surname'];
			$user->age = $_POST['age'];
			$user->sex = $_POST['sex'];
			$user->login = $_POST['login'];
			$user->password = sha1($_POST['password'].SALT);
			$user->email = $_POST['email'];	
			$user->addUser();
			header("location: /autorization_handler.php?registration=1");  //параметр get
		} 	
}
		return $errors;
	}
}