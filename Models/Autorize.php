<?php
use Models\User as User;
use Models\Validation as Validation;
use Models\Db as Db;

class Autorize
{
	public static function getAutorize()
	{
		$user = new User();
		$valid = new Validation(new Db());
		$errors = [];

		if (!empty($_SESSION['user_id'])) {
			header("location: /Main.php"); //если id уже есть, значит пользователь авторизирован
			die();
		}

		if(isset($_POST['registration'])) {
			header("location: /registration_handler.php");
			die();
		}
			
		$isRegistered = 0;

		if (!empty($_GET['registration'])) {
			$isRegistered = 1;
		} 


	if (!empty($_POST))
	{
		foreach($_POST as $k => $v){
			$valid->check($k, $v);
		}
		
		$valid->checkMaxLength("login", $_POST['login'],'users','login');
		$valid->checkMinLength("password",$_POST['password'],6);
		$errors = $valid->errors;

		$autoriz = $user->checkAutoriz($_POST['login'], sha1($_POST['password'].SALT));
		
		$_SESSION['user_id'] = $autoriz['id']; //сохраняем id юзера в сессию
		$_SESSION['login'] = $autoriz['login'];   //Вы вошли как на главной

			if(!empty($_SESSION['user_id'])) {
				header("location: /Main.php"); //если id уже есть, значит пользователь авторизирован 
			}
		}
		return $errors;
	}
}
?>