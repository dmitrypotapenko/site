<?php
namespace Models;

class Db 
{
	protected $dbConnection = null;
	private $_host = HOST;
	private $_dbname = DBNAME;
	private $_user = USER;
	private $_password = PASSWORD;
	private $_error;


	public function __construct () //конструктор класса
{
	// подключаемся к серверу
	$link = "mysql:host=".$this->_host.";dbname=".$this->_dbname.";charset=utf8";
		try 
		{
		$this->dbConnection = new \PDO($link,$this->_user, $this->_password);
		} catch (PDOException $e) {
			$this->dbConnection = null;
			$this->_error = $e->getMessage();
		}
	}

	public function geterror()
	{
		return $this->_error;//возвращаем ошибку
	}

		public function getMaxLength($table,$column)
	{
		$ins = $this->dbConnection->prepare('select COLUMN_NAME, CHARACTER_MAXIMUM_LENGTH 
			from information_schema.columns where table_schema = DATABASE() AND 
			table_name = :table AND COLUMN_NAME = :column');
		$ins->execute(array('table' => $table, 'column' => $column));
		$column = $ins->fetch(\PDO::FETCH_LAZY);
		return $column['CHARACTER_MAXIMUM_LENGTH'];
	}
}
?>