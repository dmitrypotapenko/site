<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
        <script src = "/js/jquery-3.2.1.min.js"> </script>
        <script> 

    	function funcSuccess(data) {  
    	if(data!=null){
    		console.log(data);
			$("#feed").submit();
		} else {
				$("#faildata").text("Введите правильный пароль"); 
			    }
		}

        	$(document).ready(function() {
        		$("#log_in").bind("click", function() {
        		event.preventDefault();
        		$.ajax({
        			url: "Check/check_autoriz.php",
        			type: "POST",
        			data: ({login: $("#login").val(), password: $("#password").val()}), 
        			dataType: "json",
					success: funcSuccess  			
        	});
    	});

		login.onblur = function(){
        		$.ajax({
        			url: "Check/check_login.php",
        			type: "POST",
        			data: ({login: $("#login").val()}), 
        			dataType: "html",
					success: function(data){
			this.className="errorlog";
			 if(data==1){
					errorlog.innerHTML = 'Пользователь с таким логином не зарегистрирован';
				} else errorlog.innerHTML = ''; 
			}
		});
	 };
});	
         </script>

		<title> Авторизация </title>
	</head>
	
<body>
<?php if(!empty($isRegistered)) :?>
		<h3>Вы успешно зарегистрировались. Введите свои данные для входа на главную страницу сайта</h3>
	<?php endif; ?>
	<div id = "autorization-form">
		<h2>Авторизация</h2>
			<form method="POST" autocomplete="off" action="autorization_handler.php" id="feed">
				<div style="color:red;">
					<?php 
					foreach ($errors as $error) :?>
					  <p> <?php echo $error; ?>  </p>
					  <?php endforeach; ?>
				</div>
				<div id = "errorlog" style="color:red;"> </div>	
					<div><input type="text" id = "login" name = "login" placeholder = "Логин" value="<?php echo (!empty($_POST['login']) ? 
					$_POST['login'] : '');?>"/> </div> <br>
					<div><input type="password"  id = "password" name = "password" placeholder = "Пароль"/> </div>	<br>
					<div style="color:red;" id = "faildata"></div>
					<input type="submit" name = "log" value="Log in" id = "log_in"/> 
					<input type="submit" name = "registration" value="Registration" id = "registration" required/>
			</form>
	</div>
</body>
</html>