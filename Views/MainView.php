<!DOCTYPE html>
<html>
	<head>
        <meta charset="utf-8">
		<title> Главная </title>
		  <style>
		  #comments-header {text-align: center;}
		  #comments-form {border: 1px dotted black; width: 50%; padding-left: 20px;}
		  #comments-form textarea {width: 70%; min-height: 100px; resize: none}
		  #comments-panel {border: 1px dashed black; width: 50%; padding-left: 20px; margin-top: 20px;}
		  .comment-date {font-style:italic}
  </style>
	</head>
	
<body>
<div id = "comments-header">
	<h1>Страница комментариев</h1>  <h3> <?php echo 'Вы вошли как ' . $_SESSION["login"];?>   </h3>
</div>

<div id = "comments-form">
	<h3> Добавьте комментарий </h3>
	<form method = "POST"> 
		<div> 
			<label for = "comment"> Комментарий  </label>  
			<div> 
				<textarea name = "comment" id = "comment">  </textarea> 
			</div>
			</div>
			<div>
				<br>
				<input type = "submit" name = "Save" value = "Save" />
			</div> <br>
		<div> 
			<input type = "submit" name = "Exit" value = "Exit" />
		</div>
		</form>
</div>
<div id ="comments-panel">
	<h3> Комментарии </h3>
	<?php foreach ($comments as $comment): ?>
<p> <?php if($comment['user_id'] == $_SESSION['user_id']) echo 'style="font-weight:bold;"';?><?php echo $comment['comment'];?> 
		<span class = "comment-date"> (<?php echo $comment['datetime'];?>)</span> </p>
	<?php endforeach; ?>
</div>
</body>
</html>