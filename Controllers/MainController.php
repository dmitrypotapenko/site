<?php
include_once ROOT. '/models/Main.php';

class MainController {

	public static function actionMain()
	{
		$comments = array();
		$comments =  Main::getComments();

		require_once(ROOT . '/Views/MainView.php');
		return true;
	}
}