<?php
include_once ROOT. '/models/Register.php';

class RegisterController {

	public static function actionRegister()
	{
		$mistakes = array();
		$mistakes =  Register::getRegister();

		require_once(ROOT . '/Views/RegisterView.php');
		return true;
	}
}