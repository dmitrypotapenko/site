<?php
include_once ROOT. '/models/Autorize.php';

class AutorizationController {

	public static function actionAutorize()
	{
		$errors =  Autorize::getAutorize();

		require_once(ROOT . '/Views/AutorizationView.php');
		return true;
	}
}