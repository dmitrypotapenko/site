<?php
require_once ("../config.php");

use Models\User as User;
use Models\Db as Db;

$faillog = 0;
$failemail = 0;
$db = new Db();
$user = new User();

$login = $_POST['login'];
$email = $_POST['email'];

$log;
$em;

if ($db->geterror() == "")
		{
			$log = $user->checkLogin($login);
			$em = $user->checkEmail($email);
		} 

if ($log != null) {
	 $faillog = 1;
	}

if($em != null) {
 	$failemail = 1;
 }
	
	echo json_encode(array('faillog' => $faillog, 'failemail' => $failemail)); 
?>